import schedule
import time


def job():
    print("Scrape youtube data and analyse views")


schedule.every().hour.do(job)


while True:
    schedule.run_pending()
    time.sleep(1)
