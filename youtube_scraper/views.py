from django.http import HttpResponse, JsonResponse
import json
import requests
from .models import YoutubeChannel, YoutubeVideo, Tag, YoutubeVideoTag


API_KEY = 'enter your key here'
CHANNEL_ID = 'UClcE-kVhqyiHCcjYwcpfj9w'


def get_chanel_videos(channel_id, order='date', max_results=None, api_key=API_KEY):
    url = f'https://www.googleapis.com/youtube/v3/search?key=' \
          f'{api_key}&channelId={channel_id}&part=id&order={order}'
    if max_results is not None:
        url += f'&maxResults={max_results}'
    res = requests.get(url)
    if res.status_code == 200:
        response_obj = json.loads(res.text)
        items = response_obj['items']
        return [item['id']['videoId'] for item in items]


def get_video_info(video_id, api_key=API_KEY):
    url = f'https://www.googleapis.com/youtube/v3/videos?id={video_id}&key={api_key}' \
          f'&fields=items(id,snippet(tags),statistics)&part=snippet,statistics'
    res = requests.get(url)
    if res.status_code == 200:
        response_obj = json.loads(res.text)
        items = response_obj['items'][0]
        return items


def get_video_tags(video_info):
    return video_info['snippet']['tags']


def get_video_stats(video_info):
    return video_info['statistics']


def create_channel(channel_id):
    channel_obj, _ = YoutubeChannel.objects.get_or_create(channel_id=channel_id)
    return channel_obj


def create_video(video_id, channel):
    video_obj, _ = YoutubeVideo.objects.get_or_create(
        channel=channel,
        video_id=video_id,
    )
    return video_obj


def update_video(video, statistics):
    view_count = statistics.get('viewCount')
    like_count = statistics.get('likeCount')
    dislike_count = statistics.get('dislikeCount')
    favorite_count = statistics.get('favoriteCount')
    comment_count = statistics.get('commentCount')
    video.view_count = view_count
    video.like_count = like_count
    video.dislike_count = dislike_count
    video.favorite_count = favorite_count
    video.comment_count = comment_count
    video.save()


def create_video_tags(tags, video):
    for tag_title in tags:
        tag_obj, _ = Tag.objects.get_or_create(title=tag_title)
        YoutubeVideoTag.objects.get_or_create(tag=tag_obj, video=video)


def scrape_channel_videos(request, channel_id):
    channel_videos = get_chanel_videos(channel_id, max_results=20)
    channel = create_channel(channel_id)
    for video_id in channel_videos:
        video_info = get_video_info(video_id)
        video_stats = get_video_stats(video_info)
        video = create_video(video_id, channel)
        update_video(video, video_stats)
        video_tags = get_video_tags(video_info)
        create_video_tags(video_tags, video)

    return HttpResponse(f'Done scraping videos to database!'
                        f' {len(channel_videos)} videos were created or updated')
