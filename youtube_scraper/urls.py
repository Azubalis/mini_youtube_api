from django.urls import path

from . import views

urlpatterns = [
    path('<str:channel_id>', views.scrape_channel_videos),
]
