from django.apps import AppConfig


class YoutubeScraperConfig(AppConfig):
    name = 'youtube_scraper'
