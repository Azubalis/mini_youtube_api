from django.db import models


class YoutubeChannel(models.Model):
    channel_id = models.CharField(max_length=255)


class YoutubeVideo(models.Model):
    channel = models.ForeignKey(YoutubeChannel, on_delete=models.CASCADE)
    video_id = models.CharField(max_length=255)
    view_count = models.CharField(max_length=255, null=True, blank=True)
    like_count = models.CharField(max_length=255, null=True, blank=True)
    dislike_count = models.CharField(max_length=255, null=True, blank=True)
    favorite_count = models.CharField(max_length=255, null=True, blank=True)
    comment_count = models.CharField(max_length=255, null=True, blank=True)


class Tag(models.Model):
    title = models.CharField(max_length=255)


class YoutubeVideoTag(models.Model):
    video = models.ForeignKey(YoutubeVideo, on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
