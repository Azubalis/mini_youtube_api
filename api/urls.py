from django.urls import path

from . import views

urlpatterns = [
    path('<str:tag>', views.get_videos_by_tag),
]
