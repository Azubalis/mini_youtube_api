from django.http import HttpResponse
from youtube_scraper.models import YoutubeVideo, YoutubeVideoTag, Tag
from django.core import serializers


def get_videos_by_tag(request, tag):
    print(tag)
    tag_obj = Tag.objects.filter(title=tag)
    if tag_obj:
        tag_videos_obj = YoutubeVideoTag.objects.filter(tag=tag_obj[0])
        videos = YoutubeVideo.objects.filter(
            pk__in=[yvt.video.id for yvt in tag_videos_obj]
        )
        videos_json = serializers.serialize('json', videos)
        return HttpResponse(videos_json, content_type='application/json')
    return HttpResponse("No info found")
