# from youtube_scraper.models import YoutubeVideo, YoutubeVideoTag
# from rest_framework import serializers
#
#
# class TagSerializer(serializers.HyperlinkedModelSerializer):
#
#     class Meta:
#         model = YoutubeVideoTag
#
#
# class YoutubeVideoSerializer(serializers.HyperlinkedModelSerializer):
#
#     tags = TagSerializer(read_only=True, many=True)
#
#     class Meta:
#         model = YoutubeVideo
#         fields = [
#             'tags', 'view_count', 'like_count', 'dislike_count', 'favorite_count', 'comment_count'
#         ]
#
